package com.pig.easy.bpm.dto.response;

import lombok.Data;
import lombok.ToString;

import java.time.LocalDateTime;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/6/26 17:31
 */
@Data
@ToString
public class DictDTO extends BaseResponseDTO {

    private static final long serialVersionUID = 804542365579320627L;

    private Long dictId;

    private String dictCode;

    private String dictName;

    private String tenantId;

    private String remark;

    private Integer validState;

    private Long operatorId;

    private String operatorName;

    private LocalDateTime updateTime;

    private LocalDateTime createTime;
}
