package com.pig.easy.bpm.service;

import com.pig.easy.bpm.dto.response.ConfigTemplateDTO;
import com.pig.easy.bpm.utils.Result;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author pig
 * @since 2020-05-21
 */
public interface ConfigTemplateService {


    Result<ConfigTemplateDTO> getConfigTemplate(String configKey);
}
