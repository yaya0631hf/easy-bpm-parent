package com.pig.easy.bpm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 流程表
 * </p>
 *
 * @author pig
 * @since 2020-05-20
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("bpm_process")
public class ProcessDO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 流程编码
     */
    @TableId(value = "process_id", type = IdType.AUTO)
    private Long processId;
    /**
     * 流程key
     */
    @TableField("process_key")
    private String processKey;
    /**
     * 流程名称
     */
    @TableField("process_name")
    private String processName;
    /**
     * 流程归属菜单编号
     */
    @TableField("process_menu_id")
    private Long processMenuId;
    /**
     * 流程简称
     */
    @TableField("process_abbr")
    private String processAbbr;

    /**
     * 所属公司
     */
    @TableField("company_id")
    private Long companyId;
    /**
     * 公司编码
     */
    @TableField("company_code")
    private String companyCode;
    /**
     * 租户编号
     */
    @TableField("tenant_id")
    private String tenantId;

    @TableField("process_detail_id")
    private Long processDetailId;

    /**
     * 排序
     */
    @TableField("sort")
    private Integer sort;

    @TableField("process_type")
    private Integer processType;
    /**
     * 流程状态
     */
    @TableField("process_status")
    private Integer processStatus;
    /**
     * 流程备注
     */
    private String remarks;
    /**
     * 状态 1 有效 0 失效
     */
    @TableField("valid_state")
    private Integer validState;
    /**
     * 操作人工号
     */
    @TableField("operator_id")
    private Long operatorId;
    /**
     * 操作人姓名
     */
    @TableField("operator_name")
    private String operatorName;
    /**
     * 创建时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;
    /**
     * 更新时间
     */
    @TableField("update_time")
    private LocalDateTime updateTime;


}
