package com.pig.easy.bpm.mapper;

import com.pig.easy.bpm.entity.RoleGroupToRoleDO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author pig
 * @since 2020-06-14
 */
@Mapper
public interface RoleGroupToRoleMapper extends BaseMapper<RoleGroupToRoleDO> {

}
