package com.pig.easy.bpm.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pig.easy.bpm.dto.request.MessageWhiteListQueryDTO;
import com.pig.easy.bpm.dto.request.MessageWhiteListSaveOrUpdateDTO;
import com.pig.easy.bpm.dto.response.MessageWhiteListDTO;
import com.pig.easy.bpm.entity.MessageWhiteListDO;
import com.pig.easy.bpm.entityError.EntityError;
import com.pig.easy.bpm.mapper.MessageWhiteListMapper;
import com.pig.easy.bpm.service.MessageWhiteListService;
import com.pig.easy.bpm.utils.BeanUtils;
import com.pig.easy.bpm.utils.CommonUtils;
import com.pig.easy.bpm.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
/**
 * <p>
 * 通知白名单 服务实现类
 * </p>
 *
 * @author pig
 * @since 2020-10-20
 */
@org.apache.dubbo.config.annotation.Service
public class MessageWhiteListServiceImpl extends BeseServiceImpl<MessageWhiteListMapper, MessageWhiteListDO>implements MessageWhiteListService {

        @Autowired
        MessageWhiteListMapper mapper;

        @Override
        public Result<PageInfo<MessageWhiteListDTO>>getListByCondition(MessageWhiteListQueryDTO param){

          if(param==null){
            return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
          }
          int pageIndex=CommonUtils.evalInt(param.getPageIndex(),DEFAULT_PAGE_INDEX);
          int pageSize=CommonUtils.evalInt(param.getPageSize(),DEFAULT_PAGE_SIZE);

          PageHelper.startPage(pageIndex,pageSize);
          param.setValidState(VALID_STATE);
          List<MessageWhiteListDTO>list=mapper.getListByCondition(param);
          if(list==null){
           list=new ArrayList<>();
          }
          PageInfo<MessageWhiteListDTO>pageInfo=new PageInfo<>(list);
          return Result.responseSuccess(pageInfo);
        }


        @Override
        public Result<Integer>insertMessageWhiteList(MessageWhiteListSaveOrUpdateDTO param){

          if(param==null){
            return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
          }

          MessageWhiteListDO temp=BeanUtils.switchToDO(param, MessageWhiteListDO.class);
            Integer num=mapper.insert(temp);
            return Result.responseSuccess(num);
        }

         @Override
         public Result<Integer>updateMessageWhiteList(MessageWhiteListSaveOrUpdateDTO param){

          if(param==null){
           return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
          }

          MessageWhiteListDO temp=BeanUtils.switchToDO(param, MessageWhiteListDO.class);
          Integer num=mapper.updateById(temp);
          return Result.responseSuccess(num);
        }

        @Override
        public Result<Integer>deleteMessageWhiteList(MessageWhiteListSaveOrUpdateDTO param){

          if(param==null){
             return Result.responseError(EntityError.ILLEGAL_ARGUMENT_ERROR);
          }

          MessageWhiteListDO temp=BeanUtils.switchToDO(param, MessageWhiteListDO.class);
          temp.setValidState(INVALID_STATE);
          Integer num=mapper.updateById(temp);
          return Result.responseSuccess(num);
        }

}
