package com.pig.easy.bpm.vo.request;

import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/7/1 14:46
 */
@Data
@ToString
public class HistoryVO implements Serializable {

    private static final long serialVersionUID = 327375894878104872L;

    private Long historyId;

    /**
     * 申请编号
     */
    @NotNull
    private Long applyId;

    /**
     * 任务编号
     */
    @NotNull
    private Long taskId;

    /**
     * 任务名称
     */
    private String taskName;

    /**
     * 租户编号
     */
    private String tenantId;

    /**
     * 来源系统
     */
    private String system;

    /**
     * 来源平台
     */
    private String paltform;

    /**
     * 审批动作编码
     */
    private String approveActionCode;

    /**
     * 审批动作名称
     */
    private String approveActionName;

    /**
     * 审批意见
     */
    @NotNull
    private String approveOpinion;

    /**
     * 备注
     */
    private String remarks;


}
