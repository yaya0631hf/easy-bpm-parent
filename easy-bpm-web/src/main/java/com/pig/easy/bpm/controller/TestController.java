package com.pig.easy.bpm.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.pig.easy.bpm.service.TestService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * todo:
 *
 * @author : pig
 * @date : 2020/5/7 18:37
 */
@RestController
@RequestMapping("test")
public class TestController {

    //@NacosValue(value = "${dubbo.server.name:}", autoRefreshed = true)
    private String serverName;

    @Reference
    private TestService testService;

    @RequestMapping("getServerName")
    public String getServerName(){

        System.out.println("serverName = " + serverName);
        return testService.getTest();
    }

}
