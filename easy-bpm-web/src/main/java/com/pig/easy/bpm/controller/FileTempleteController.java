package com.pig.easy.bpm.controller;


import com.alibaba.dubbo.config.annotation.Reference;
import com.github.pagehelper.PageInfo;
import com.pig.easy.bpm.constant.BpmConstant;
import com.pig.easy.bpm.dto.request.FileTempleteQueryDTO;
import com.pig.easy.bpm.dto.request.FileTempleteSaveOrUpdateDTO;
import com.pig.easy.bpm.dto.response.FileTempleteDTO;
import com.pig.easy.bpm.entityError.EntityError;
import com.pig.easy.bpm.service.FileTempleteService;
import com.pig.easy.bpm.utils.BestBpmAsset;
import com.pig.easy.bpm.utils.JsonResult;
import com.pig.easy.bpm.utils.Result;
import com.pig.easy.bpm.vo.request.FileTempleteQueryVO;
import com.pig.easy.bpm.vo.request.FileTempleteSaveOrUpdateVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;


/**
 * <p>
 * 模板文件表 前端控制器
 * </p>
 *
 * @author pig
 * @since 2020-08-10
 */
@RestController
@Api(tags = "模板文件表管理", value = "模板文件表管理")
@RequestMapping("/fileTemplete")
public class FileTempleteController extends BaseController {

    @Reference
    FileTempleteService service;

    @ApiOperation(value = "查询模板文件表列表", notes = "查询模板文件表列表", produces = "application/json")
    @PostMapping("/getList")
    public JsonResult getList(@Valid @RequestBody FileTempleteQueryVO param) {

        BestBpmAsset.isAssetEmpty(param);
        BestBpmAsset.isAssetEmpty(param.getTenantId());
        FileTempleteQueryDTO queryDTO = switchToDTO(param, FileTempleteQueryDTO.class);

        Result<PageInfo<FileTempleteDTO>> result = service.getListByCondition(queryDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "新增模板文件表", notes = "新增模板文件表", produces = "application/json")
    @PostMapping("/insert")
    public JsonResult insertFileTemplete(@Valid @RequestBody FileTempleteSaveOrUpdateVO param) {

        BestBpmAsset.isAssetEmpty(param);
        BestBpmAsset.isAssetEmpty(param.getTenantId());
        FileTempleteSaveOrUpdateDTO saveDTO = switchToDTO(param, FileTempleteSaveOrUpdateDTO.class);
        saveDTO.setOperatorId(currentUserInfo().getUserId());
        saveDTO.setOperatorName(currentUserInfo().getRealName());

        Result<Integer> result = service.insertFileTemplete(saveDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "修改模板文件表", notes = "修改模板文件表", produces = "application/json")
    @PostMapping("/update")
    public JsonResult updateFileTemplete(@Valid @RequestBody FileTempleteSaveOrUpdateVO param) {

        BestBpmAsset.isAssetEmpty(param);
        BestBpmAsset.isAssetEmpty(param.getTenantId());
        FileTempleteSaveOrUpdateDTO saveDTO = switchToDTO(param, FileTempleteSaveOrUpdateDTO.class);
        saveDTO.setOperatorId(currentUserInfo().getUserId());
        saveDTO.setOperatorName(currentUserInfo().getRealName());

        Result<Integer> result = service.updateFileTemplete(saveDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

    @ApiOperation(value = "删除模板文件表", notes = "删除模板文件表", produces = "application/json")
    @PostMapping("/delete")
    public JsonResult delete(@Valid @RequestBody FileTempleteSaveOrUpdateVO param) {

        BestBpmAsset.isAssetEmpty(param);
        BestBpmAsset.isAssetEmpty(param.getTenantId());
        FileTempleteSaveOrUpdateDTO saveDTO = switchToDTO(param, FileTempleteSaveOrUpdateDTO.class);
        saveDTO.setOperatorId(currentUserInfo().getUserId());
        saveDTO.setOperatorName(currentUserInfo().getRealName());
        saveDTO.setValidState(BpmConstant.INVALID_STATE);

        Result<Integer> result = service.deleteFileTemplete(saveDTO);
        if (result.getEntityError().getCode() != EntityError.SUCCESS.getCode()) {
            return JsonResult.error(result.getEntityError());
        }
        return JsonResult.success(result.getData());
    }

}

